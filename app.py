# from flask import Flask, request, jsonify
# from sklearn import tree
# import pandas as pd
# import numpy as np

# app = Flask(__name__)


# @app.route("/predict/stunting", methods=["POST"])
# def predict():
#     data = request.json
#     input_data = np.array(data["input"])
#     prediction = clf.predict([input_data])
#     return jsonify({"prediction": prediction[0]})


# if __name__ == "__main__":
#     stunting_df = pd.read_excel("data/stunting.xlsx")
#     sex = {"jenis_kelamin": {"laki-laki": 0, "perempuan": 1}}
#     stunting_df.replace(sex, inplace=True)
#     one_hot_data = stunting_df[
#         ["jenis_kelamin", "bulan", "berat_badan", "tinggi_badan"]
#     ]
#     clf = tree.DecisionTreeClassifier()
#     clf_train = clf.fit(one_hot_data, stunting_df["stunting"])
#     app.run()

from flask import Flask, request, jsonify
from sklearn import tree
import pandas as pd
import numpy as np
import dropbox

app = Flask(__name__)

# Kredensial Dropbox API
ACCESS_TOKEN = "sl.BiD8XIdf7l6OQy7l_Walivufx-LYVR4GdVDO83ngvfhPyq49rqlcRDFll_GAf3V3bzIvxRO4JneEN5mEVTE85MGWu-RLqbEbGdaQFjvu-TXxipXXaOghrQ9k67NZ9cDuUSEcoPU"

# Path file Dropbox
EXCEL_FILE_PATH = "/stunting.xlsx"


@app.route("/predict/stunting", methods=["POST"])
def predict():
    data = request.json
    input_data = np.array(data["input"])
    prediction = clf.predict([input_data])
    return jsonify({"prediction": prediction[0]})


if __name__ == "__main__":
    # Mengakses file Excel dari Dropbox
    dbx = dropbox.Dropbox(ACCESS_TOKEN)
    _, res = dbx.files_download(EXCEL_FILE_PATH)
    excel_data = res.content

    # Memuat data Excel ke dalam DataFrame pandas
    stunting_df = pd.read_excel(excel_data)

    # Bagian kode lainnya
    sex = {"jenis_kelamin": {"laki-laki": 0, "perempuan": 1}}
    stunting_df.replace(sex, inplace=True)
    one_hot_data = stunting_df[["jenis_kelamin",
                                "bulan", "berat_badan", "tinggi_badan"]]
    clf = tree.DecisionTreeClassifier()
    clf_train = clf.fit(one_hot_data, stunting_df["stunting"])

    app.run()
